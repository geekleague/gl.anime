import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ProTip from '../src/ProTip';
import Link from '../src/Link';
import Page from '../layouts/main';

import Carousel from '../components/SimpleSlider';
import Banner from '../components/Banner';
import "slick-carousel/slick/slick.css";

export default function Index() {
  return (
    <Page>
      <Container>
        <Carousel />
        <Banner />
        <Box my={4}>
          <Typography variant="h4" component="h1" gutterBottom>
            Next.js example
          </Typography>
          <Link href="/about" color="secondary">
            Go to the about page
          </Link>
          <ProTip />
        </Box>
      </Container>
    </Page>
  );
}
