// import Meta from '../components/Meta'
import Head from '../components/Head'
import Footer from '../components/Footer'
// import Grid from '@material-ui/core/Grid';

export default ({ children }) => (
  <div className="flexGrow" >
    <Head />
    { children }
    <Footer />
  </div>
)