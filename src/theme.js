import { createMuiTheme } from '@material-ui/core/styles';
import { red, purple, indigo } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: purple,
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
