const withSass = require('@zeit/next-sass');
const withCSS = require("@zeit/next-css");
module.exports = withCSS(withSass({
    lessLoaderOptions: {
        javascriptEnabled: true,
    },
    webpack (config, options) {
        config.module.rules.push({
            test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
            loader: 'eslint-loader',
            exclude: ['/node_modules/', '/.next/', '/out/'],
            enforce: 'pre',
            options: {
                emitWarning: false,
            },
        });

        return config;
    }
}));