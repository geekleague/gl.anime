import Head from 'next/head'

function IndexPage() {
  return (
    <div>
      <Head>
        <title>gLAnime</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
        />
      </Head>
    </div>
  )
}

export default IndexPage