import React from "react";
import Slider from "react-slick";

function SampleArrow(props) {
  const { style } = props;
  return (
    <div
      style={{ ...style, display: "none" }}
    />
  );
  }

class SimpleSlider extends React.Component {
  render() {
    var settings = {
      dots: false,
      infinite: true,
      slidesToShow: 7,
      slidesToScroll: 2,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 2000,
      cssEase: "linear",
      nextArrow: <SampleArrow />,
      prevArrow: <SampleArrow />,
      customPaging: function(i) {
        return (
          <SampleArrow />
        );
      },
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 600,
          settings: {
            speed: 700,
            slidesToShow: 4,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <div className="container-slick">
        <Slider {...settings}>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/1.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/2.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/3.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/4.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/5.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/6.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/7.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/8.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/6.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/7.jpg" />
            <span>Titulo del anime</span>
          </div>
          <div className="Item__ccarousel">
            <img className="imgCarousel" src="/static/image/anime/8.jpg" />
            <span>Titulo del anime</span>
          </div>
        </Slider>
      </div>
    );
  }
}

export default SimpleSlider;
